# frozen_string_literal: true

require 'open-uri'
require 'icalendar'
require 'erb'
require 'optimist'
require 'dotenv'
require 'mail'

# Load environment variables set in .env files
Dotenv.load

# Read command line options
options = Optimist::options do
  opt :email, "Send emails"
  opt :template, "Path to ERB template file", :type => :string, :required => true
  opt :calendars, "URIs where iCal files are located", :type => :strings, :required => true
  opt :output, "Redirect output to this file", :type => :string
  opt :offset, "Number of days from now to look for events, default is 7", :type => :integer
end

# Set up filters for events
if options[:offset].nil?
  options[:offset] = 7
end

now = DateTime.now
future_date = now + options[:offset]

# Load iCal files
events = Array.new
options[:calendars].each do |calendar|
  ical_file = URI.open calendar
  these_events = Icalendar::Event.parse(ical_file)
  these_events = these_events.select { |event| event.dtstart > now and event.dtstart < future_date }
  events << these_events.sort_by(&:dtstart)
end

# Load ERB template
file = File.open options[:template]
template = file.read
file.close

# Generate message from ERB template
message = ERB.new template, trim_mode: "%<>"
message = message.result

# binding.break

# Write options to file
if options[:email]
  Mail.defaults do
    delivery_method :smtp, { address:              ENV['TOKI_SONA_SMTP_HOST'],
                             port:                 ENV['TOKI_SONA_SMTP_PORT'],
                             domain:               ENV['TOKI_SONA_SMTP_DOMAIN'],
                             user_name:            ENV['TOKI_SONA_SMTP_USER_NAME'],
                             password:             ENV['TOKI_SONA_SMTP_PASSWORD'],
                             authentication:       ENV['TOKI_SONA_SMTP_AUTHENTICATION'],
                             enable_starttls_auto: ENV['TOKI_SONA_ENABLE_STARTTLS_AUTO'] }
  end
  Mail.deliver do
    to ENV['TOKI_SONA_TO']
    from ENV['TOKI_SONA_FROM']
    subject ENV['TOKI_SONA_SUBJECT']

    html_part do
      content_type 'text/html; charset=UTF-8'
      body message
    end
  end
elsif !options[:output].nil?
  File.write options[:output], message
else
  puts message
end
